# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  ans = (1..100).to_a.sample
  guesses = 0
  puts 'guess a number between 1 and 100:'
  $stdin.each do |input|
    guesses += 1
    guess = input.to_i
    if guess > ans
      puts "your guess (#{input}) is too high. try again:"
    elsif guess < ans
      puts "your guess (#{input}) is too low. try again:"
    elsif guess == ans
      puts "You Guessed It!: #{input}. It took you #{guesses} tries"
      break
    end
  end
end
